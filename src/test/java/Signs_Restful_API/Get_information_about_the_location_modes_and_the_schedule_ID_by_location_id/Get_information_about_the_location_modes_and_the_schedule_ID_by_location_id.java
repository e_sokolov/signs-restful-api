package Signs_Restful_API.Get_information_about_the_location_modes_and_the_schedule_ID_by_location_id;

import Signs_Restful_API.API_base;
import TestRailApi.gurock.testrail.TestListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@Listeners({TestListener.class})

public class Get_information_about_the_location_modes_and_the_schedule_ID_by_location_id extends API_base {

    @Test(priority = 1)
    public void get_information_with_NULL_in_x_api_key_header() {
        String api_route = property.getProperty("main_URL_Of_API") +
                property.getProperty("Get_information_about_the_location_modes_and_the_schedule_ID_by_location_id") +
                property.getProperty("location_id");
        System.out.println("\n" + api_route);
        given()
                .header(property.getProperty("header_key_X-Requested-With"),property.getProperty("header_value_X-Requested-With"))
                .header(property.getProperty("header_key_Content-Type"),property.getProperty("header_value_Content-Type"))
                .header(property.getProperty("header_key_x-api-key"),"")
                .get(api_route)
                .then().statusCode(Integer.parseInt(property.getProperty("status_Code_Unauthorized")))
                .body(equalTo(property.getProperty("valid_Response_Of_Negativ_Tests_Pass_Unauthorized")))
                .log().all();
    }

    @Test(priority = 2)
    public void get_information_with_Wrong_x_api_key_header() {
    }

    @Test(priority = 3)
    public void get_information_with_Expired_x_api_key_header() {
    }

    @Test(priority = 4)
    public void get_information_from_other_company() {
    }

    @Test(priority = 5)
    public void get_information_about_the_location_mode() {
    }

    @Test(priority = 6)
    public void get_information_about_the_schedule() {
    }

    @Test(priority = 7)
    public void get_schedule_from_nonexistent_location_id() {
    }

    @Test(priority = 8)
    public void get_schedule_from_nonexistent_location_id_alphabetic() {
    }

    @Test(priority = 9)
    public void get_schedule_from_location_id_NULL() {
    }
}