package Signs_Restful_API;

import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import static java.lang.System.getProperty;

public class API_base {

    @BeforeTest
    public void setUp() throws IOException {
        setProperty();
    }

    public static Properties property = new Properties();

    public void setProperty() throws IOException {
        String target = getProperty("target", "Signs_API_Test_Environment");
        property.load(new FileReader(new File(String.format("C:\\WD2\\Signs_Restful_API\\src\\test\\resources\\%s.properties", target))));
    }
}
