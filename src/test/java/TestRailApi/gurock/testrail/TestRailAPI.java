package TestRailApi.gurock.testrail;

import Signs_Restful_API.API_base;
import com.github.cliftonlabs.json_simple.JsonObject;

import java.util.HashMap;
import java.util.Map;

public class TestRailAPI {

    public static void addResultForCase(String methodName, int statusId, String commentForStatus) {

        String idTestRun = API_base.property.getProperty("number_of_testrun");
        //String idTestRun = "66";

        Map<String, Integer> listTestsById = new HashMap<>();

        APIClient client = new APIClient("https://lxw.testrail.net/");    //initialize TestRail
        client.setUser("test.fintegro@gmail.com");
        client.setPassword("Fintegro");

        /** Get information about the location modes and the schedule ID by location_id **/

        listTestsById.put("get_information_with_NULL_in_x_api_key_header", 35894);
        listTestsById.put("get_information_with_Wrong_x_api_key_header", 35895);
        listTestsById.put("get_information_with_Expired_x_api_key_header", 35896);
        listTestsById.put("get_information_from_other_company", 35897);
        listTestsById.put("get_information_about_the_location_mode", 35898);
        listTestsById.put("get_information_about_the_schedule", 35899);
        listTestsById.put("get_schedule_from_nonexistent_location_id", 35900);
        listTestsById.put("get_schedule_from_nonexistent_location_id_alphabetic", 35901);
        listTestsById.put("get_schedule_from_location_id_NULL", 35902);

        /** Get information about the location modes and the schedule ID by location_id **/


        Map<String, java.io.Serializable> data = new HashMap<>();
        data.put("status_id", statusId);                         // status of the test-case: 1 PASS, 5 FAIL, 3 UNTESTED
        data.put("comment", commentForStatus);
        try {
            JsonObject r = (JsonObject) client.sendPost("add_result_for_case/" + idTestRun
                    + "/" + listTestsById.get(methodName), data);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}