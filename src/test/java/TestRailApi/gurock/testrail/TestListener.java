package TestRailApi.gurock.testrail;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestListener implements ITestListener {                   //класс расширяет/меняет библиотеку ITestListener

    @Override
    public void onTestStart(ITestResult result) {
    }

    @Override
    public void onTestSuccess(ITestResult result) {
//        System.out.println(String.format("PASS %s.%s", result.getTestClass().getName(), result.getMethod().getMethodName()));

        TestRailAPI.addResultForCase(result.getMethod().getMethodName(), 1, "Test PASS");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        TestRailAPI.addResultForCase(result.getMethod().getMethodName(), 5, "Test FAIL");
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        TestRailAPI.addResultForCase(result.getMethod().getMethodName(), 3, "Test UNTESTED");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
    }

    @Override
    public void onStart(ITestContext context) {
    }

    @Override
    public void onFinish(ITestContext context) {
    }
}

